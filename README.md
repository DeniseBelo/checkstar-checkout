# CheckStar Checkout System:

It is a simple managing products and checkout service which allows the user to choose the products, add them to cart, remove the products from cart, see the total value of the purchase, how much he saved and the real amount to pay. Because of the time, I couldn't integrate it with the back-end made for this application, so the values are fixed in a context so that you can see and play with the features. I am really sorry for that, it would be very nice to see all together and running.

## How to get started:

To run this service in your local you need to do some steps, starting with cloning this project from gitlab. Click on this link:

```
https://gitlab.com/DeniseBelo/checkstar-checkout
```

Clone with the ssh option. After that, create a new diretory in your local, inside this directory use:

```
git clone <paste the url copied>
```

Move in the directory created and run you code editor (you can use visual studio code, for instance).
Now open you terminal inside de project and if you have yarn previously installed in you machine, run:

```
yarn install
```

You will install all the dependencies you need this way. And now you can see the code.

### In case you don't have yarn install follow these steps:

Yarn installation. In your terminal type the following code:

```
npm install --global yarn
```

With yarn installed go to the directory of the project:

```
cd <your_project>
```

Run the code on your local. Open a terminal inside your project directory, and install the project dependencies:

```
yarn install
```

Run the application:

```
yarn start
```

Now you have to use navigator to select your products. Click on the cart to see your selected items and click on the logo checkstar to return to the showcase and add more items.

## Technologies

- React.js;
- Context;
- Javascript.

## Licenses

MIT
