import ProductCardContainer from "./styles";
import { useCart } from "../../Providers/CartProvider";

const ProductCardCart = ({ product, removeFromCart }) => {
  return (
    <ProductCardContainer>
      <figure className="product-image-container">
        <img className="product-image" src={product.image} alt={product.name} />
        <figcaption className="product-name"> {product.name} </figcaption>
      </figure>
      <p className="product-price"> {product.price} </p>
      <button onClick={() => removeFromCart(product.id)}> Remove item </button>
    </ProductCardContainer>
  );
};

export default ProductCardCart;
