import styled from "styled-components";

const ProductCardContainer = styled.section`
  background-color: white;
  width: 90vw;
  max-width: 400px;
  border-radius: 5px;
  align-items: center;
  display: flex;
  justify-content: space-around;
  margin: 10px;

  .product-image {
    width: 80px;
    height: 80px;
  }

  .product-price {
    color: red;
    font-weight: bold;
  }

  button {
    background-color: #a5a5a5;
    padding: 10px;
  }
`;

export default ProductCardContainer;
