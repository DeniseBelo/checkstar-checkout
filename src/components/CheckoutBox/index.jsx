import { CheckoutContainer, FullPrice, Discount, TotalPrice } from "./styles";
import { useCart } from "../../Providers/CartProvider";
import { useEffect, useState } from "react";

const CheckoutBox = () => {
  const { cartList, setCartList } = useCart();
  const [fullPrice, setFullPrice] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [totalAmountToPay, setTotalAmountToPay] = useState(0);

  useEffect(() => {
    const fullPrice = cartList.reduce(
      (fullPrice, product) => fullPrice + product.price,
      0
    );
    setFullPrice(fullPrice);
    const totalAmountToPay = cartList.reduce(
      (totalAmountToPay, product) => totalAmountToPay + product.discountPrice,
      0
    );
    setTotalAmountToPay(totalAmountToPay.toFixed(2));
    const discount = fullPrice - totalAmountToPay;
    setDiscount(discount.toFixed(2));
  }, [cartList]);

  return (
    <CheckoutContainer>
      <FullPrice>
        <h3>Full Price: </h3>
        <h4>£{fullPrice}</h4>
      </FullPrice>
      <Discount>
        <h3>Saved: </h3>
        <h4>£{discount}</h4>
      </Discount>
      <TotalPrice>
        <h3>Total: </h3>
        <h4>£{totalAmountToPay}</h4>
      </TotalPrice>
    </CheckoutContainer>
  );
};

export default CheckoutBox;
