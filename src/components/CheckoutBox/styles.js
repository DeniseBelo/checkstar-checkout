import styled from "styled-components";

export const CheckoutContainer = styled.div`
  width: 300px;
  height: 400px;
  background-color: #b3b3b3;
  align-items: center;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  border: 2px solid black;
  //flex-wrap: wrap;
  //box-sizing: border-box;
`;

export const FullPrice = styled.div`
  background-color: #b6b6b6;
  align-items: center;
  box-sizing: border-box;
  border-bottom: 1px solid black;
`;

export const Discount = styled.p`
  background-color: ##b6b6b6;
  color: red;
  align-items: center;
  box-sizing: border-box;
  border-bottom: 1px solid black;
`;

export const TotalPrice = styled.div`
  background-color: ##b6b6b6;
  align-items: center;
  box-sizing: border-box;
`;
