import ProductShowcaseContainer from "./styles";
import { useCart } from "../../Providers/CartProvider";
import { useEffect } from "react";

const ProductCardShowcase = ({ product, addToCart }) => {
  const { cartList, setCartList } = useCart();

  return (
    <ProductShowcaseContainer>
      <figure className="product-image-container">
        <img className="product-image" src={product.image} alt={product.name} />
        <figcaption className="product-name"> {product.name} </figcaption>
      </figure>
      <p className="product-description"> {product.description} </p>
      <p className="product-price"> {product.price} </p>
      <button onClick={() => addToCart(product)}> Select item </button>
    </ProductShowcaseContainer>
  );
};

export default ProductCardShowcase;
