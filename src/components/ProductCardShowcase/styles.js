import styled from "styled-components";

const ProductShowcaseContainer = styled.section`
  background-color: white;
  width: 90vw;
  max-width: 200px;
  border-radius: 5px;
  align-items: center;
  text-align: center;
  padding: 10px;
  margin-top: 10px;

  .product-image {
    width: 80px;
    height: 80px;
  }

  .product-price {
    color: red;
    font-weight: bold;
  }

  button {
    background-color: #a5a5a5;
    padding: 10px;
    border-radius: 5px;
  }
`;

export default ProductShowcaseContainer;
