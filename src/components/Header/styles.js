import styled from "styled-components";

const HeaderContainer = styled.header`
  background-color: #a5a5a5;
  width: 100vw;
  display: flex;
  justify-content: space-between;
  padding: 10px 80px 10px 30px;
  border-bottom: 2px black solid;
  align-items: center;

  .logo {
    font-size: 30px;
    color: black;
  }
  .logo:hover {
    animation: shake 0.2s;
    animation-iteration-count: 4;
    cursor: pointer;
  }
  @keyframes shake {
    0% {
      margin-left: 0;
    }
    25% {
      margin-left: 5px;
    }
    50% {
      margin-left: 0;
    }
    75% {
      margin-left: -5px;
    }
    100% {
      margin-left: 0;
    }
  }
  .icon {
    font-size: 25px;
  }

  .cart {
    font-size: 20px;
    display: flex;
    cursor: pointer;
  }
`;
export default HeaderContainer;
