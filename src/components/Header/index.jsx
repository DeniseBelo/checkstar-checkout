import { GiShoppingCart } from "react-icons/gi";
import HeaderContainer from "./styles";
import { useNavigate } from "react-router-dom";

const Header = () => {
  const navigate = useNavigate();

  const handleCartIcon = () => {
    navigate("/cart");
  };

  const handleLogoIcon = () => {
    navigate("/");
  };

  return (
    <HeaderContainer>
      <div className="logo" onClick={handleLogoIcon}>
        CheckStar
      </div>
      <div className="cart" onClick={handleCartIcon}>
        <GiShoppingCart className="icon" />
        <div>Cart</div>
      </div>
    </HeaderContainer>
  );
};

export default Header;
