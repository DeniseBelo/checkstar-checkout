import { Routes, Route } from "react-router-dom";
import Showcase from "../pages/Showcase";
import Cart from "../pages/Cart";

const RouterPath = () => {
  return (
    <Routes>
      <Route path="/" element={<Showcase />} />
      <Route path="/cart" element={<Cart />} />
    </Routes>
  );
};

export default RouterPath;
