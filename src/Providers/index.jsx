import { CartProvider } from "./CartProvider";
import { ProductsListProvider } from "./ProductsListProvider";

const Providers = ({ children }) => {
  return (
    <ProductsListProvider>
      <CartProvider>{children}</CartProvider>
    </ProductsListProvider>
  );
};

export default Providers;
