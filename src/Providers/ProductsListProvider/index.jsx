import { createContext, useContext, useState } from "react";

const ProductsListContext = createContext();

export const ProductsListProvider = ({ children }) => {
  const [productsList, setProductsList] = useState([
    {
      id: 1,
      image:
        "https://img.itdg.com.br/tdg/images/blog/uploads/2019/05/pizza.jpg",
      name: "Amazing Pizza",
      price: 39.99,
      discountPrice: 32.99,
      description: "Pepperoni and cheese",
    },
    {
      id: 2,
      image:
        "https://media-cdn.tripadvisor.com/media/photo-s/1c/68/34/58/carro-chefe-da-casa-criado.jpg",
      name: "Amazing Burger",
      price: 29.99,
      discountPrice: 22.99,
      description: "Mix of meat, jalapenos, cheese and tomatoes",
    },
    {
      id: 3,
      image:
        "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/french-fries-royalty-free-image-1634646257.jpg",
      name: "Boring Fries",
      price: 10.99,
      discountPrice: 6.99,
      description: "Fresh potatoes",
    },
    {
      id: 4,
      image:
        "https://images.immediate.co.uk/production/volatile/sites/30/2014/05/Epic-summer-salad-hub-2646e6e.jpg",
      name: "Amazing Salad",
      price: 12.99,
      discountPrice: 8.99,
      description: "Lettuce, tomatoes, onions, garlic and sauce",
    },
  ]);

  return (
    <ProductsListContext.Provider value={{ productsList, setProductsList }}>
      {children}
    </ProductsListContext.Provider>
  );
};

export const useProductsList = () => useContext(ProductsListContext);
