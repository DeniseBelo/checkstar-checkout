import { useEffect } from "react";
import { useCart } from "../../Providers/CartProvider";
import { CartContainer, ProductsContainer, MainContainer } from "./styles";
import Header from "../../components/Header";
import ProductCardCart from "../../components/ProductCardCart";
import CheckoutBox from "../../components/CheckoutBox";
const Cart = () => {
  const { cartList, setCartList } = useCart();

  const removeFromCart = (id) => {
    let correctIndex = -1;
    cartList.forEach((item, itemIndex) => {
      if (item.id === id) {
        correctIndex = itemIndex;
        return;
      }
    });
    if (correctIndex !== -1) {
      const newList = [...cartList];
      newList.splice(correctIndex, 1);
      setCartList(newList);
    }
    return;
  };

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartList));
  }, [cartList]);

  return (
    <CartContainer>
      <Header />
      <MainContainer>
        <ProductsContainer>
          {cartList.map((product, index) => (
            <ProductCardCart
              removeFromCart={removeFromCart}
              product={product}
              key={index}
            />
          ))}
        </ProductsContainer>
        <CheckoutBox />
      </MainContainer>
    </CartContainer>
  );
};

export default Cart;
