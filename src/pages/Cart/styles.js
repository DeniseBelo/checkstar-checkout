import styled from "styled-components";

export const CartContainer = styled.div`
  background-color: #a9a8f8;
  width: 100vw;
  height: 100vh;
  //display: flex;
`;

export const MainContainer = styled.div`
  background-color: #a9a8f8;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export const ProductsContainer = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  max-height: 500px;
`;
