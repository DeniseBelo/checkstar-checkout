import { useEffect, useState } from "react";
import { useCart } from "../../Providers/CartProvider";
import { useProductsList } from "../../Providers/ProductsListProvider";
import { ShowcaseContainer } from "./styles";
import Header from "../../components/Header";
import ProductCardShowcase from "../../components/ProductCardShowcase";

const Showcase = () => {
  const { cartList, setCartList } = useCart();
  const { productsList, setProductsList } = useProductsList();

  const addToCart = (product) => {
    setCartList([...cartList, product]);
  };

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartList));
  }, [cartList]);

  return (
    <ShowcaseContainer>
      <Header />
      <div className="cartList">
        {productsList.map((product, index) => (
          <ProductCardShowcase
            addToCart={addToCart}
            product={product}
            key={index}
          />
        ))}
      </div>
    </ShowcaseContainer>
  );
};

export default Showcase;
