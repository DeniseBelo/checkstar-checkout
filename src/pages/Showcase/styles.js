import styled from "styled-components";

export const ShowcaseContainer = styled.div`
  background-color: #a9a8f8;
  width: 100vw;
  height: 100vh;

  .cartList {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
  }
`;
